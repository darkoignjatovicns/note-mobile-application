package com.darkoignjatovic.note.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.darkoignjatovic.note.dao.NoteDao;
import com.darkoignjatovic.note.entity.Note;
import com.darkoignjatovic.note.utils.NoteUtils;

import java.util.Date;

// Whenever we make changes to our database we have to increment this version number and provide migration strategy(But we don't do migration) https://medium.com/androiddevelopers/7-steps-to-room-27a5fe5f99b2
// Because in our production our schema when was changed we uninstalled and reinstalled
@Database(entities = {Note.class}, version = 4, exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {

    // Singleton: We can't create multiple instances of this database, instead we always use the same instance everywhere in our app
    private static NoteDatabase instance;

    // Access our dao and provide body (Room takes care of the code)
    // Since we create Note database instance with builder Room auto generates all the necessary code for this method
    public abstract NoteDao noteDao();

    // Create database

    /**
     * We create only single database instance, and then we call this method from dao outside and get to handle to this instance.
     * Synchronized means only one thread at the time can access method, in this way you don't accidentally create two instances of this
     * database when two different threads try to access this method at the same time, because this can happened in the multi thread
     * environment.
     */
    public static synchronized NoteDatabase getInstance(Context context){
        if(instance == null ) {
            // This method will be called the first time when we create database but not any time after that
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    NoteDatabase.class, "note_database")
                    .fallbackToDestructiveMigration()
                    .build(); // Return instance of this database

                      //So if we increment this version number, you will just start new database in our case
        }
        return instance;
    }





}
