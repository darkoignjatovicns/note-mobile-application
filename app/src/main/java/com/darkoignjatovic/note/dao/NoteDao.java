package com.darkoignjatovic.note.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.darkoignjatovic.note.entity.Note;

import java.util.List;

@Dao
public interface NoteDao {

    @Insert
    void insert(Note note);

    @Update
    void update(Note note);

    @Delete
    void delete(Note note);

    @Query("DELETE  FROM note_table")
    void deleteAllNotes();

    /**
     * In compile time room will check if the columns of this table(note_table) fit to this(Note) java object in the List<>
     * LiveData - We can observe List<Note> - object as soon as any changes  in our note_table. This value(LiveData..) will
     * be automatically updated and our activity will be notified. And room takes all the necessary stuff to update this LiveData object.
     * We have to just declare with LiveData
     */

    // The room autogenerates the code for this method
    @Query("SELECT * FROM note_table n ORDER BY datetime(n.created) DESC")
    LiveData<List<Note>> getAllNotes();

}
