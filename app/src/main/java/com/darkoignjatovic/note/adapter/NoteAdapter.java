package com.darkoignjatovic.note.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darkoignjatovic.note.R;
import com.darkoignjatovic.note.entity.*;
import com.darkoignjatovic.note.utils.NoteUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the layout we want to use for the single item in our recycle view
 */

// Recycler knows that we want to use ViewHolder
public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {

    /**
     * Important to initialize ArrayList<>() here because otherwise notes will be null before we get first live data update
     * If that is null, we can never call these methods in this adapter
     */

    private List<Note> notes = new ArrayList<>();
    private OnItemClickListener listener; // Interface

    // Create and return NoteHolder
    @NonNull
    @Override                            // Parent is a ViewGroup, we passed here (Main Activity)
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()) // We can get a context from main activity
                .inflate(R.layout.note_item, parent, false); // Because this is the layout we want to use
        return new NoteHolder(itemView);
    }

    // This method take cares of getting the data from the single note java Objects into the views of our NoteHolder
    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
        Note currentNote = notes.get(position);
        holder.textViewTitle.setText(currentNote.getTitle());
        holder.textViewDescription.setText(currentNote.getDescription());
        holder.textViewCreated.setText(currentNote.getCreated());
        holder.textViewAddress.setText(currentNote.getAddress());
    }

    // How many items we want to display in our recycler view
    @Override
    public int getItemCount() {
        return notes.size();
    }

    // In main activity we observe the live data and in the onChange callback method we can pass the list of notes here
    public void setNotes(List<Note> notes) {
        this.notes = notes;
        notifyDataSetChanged(); // we want to notify our adapter that we had change data in our array(notes)
    }

    // We can get note from this adaptor to the outside
    public Note getNoteAt(int position) {
        return notes.get(position);
    }


    // view items
    class NoteHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewDescription;
        private TextView textViewCreated;
        private TextView textViewAddress;


        public NoteHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            textViewCreated = itemView.findViewById(R.id.text_view_date);
            textViewAddress = itemView.findViewById(R.id.text_view_address);

            // Added for update
            /**
             * Click on itemView, we get adapter's position of the clicked item, then we call onItemClick and we pass note's position
             */
            // This way to get note to the main activity
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    // listener != null -  Because it's not guarantee that we will ever call setOnItemClickListener method from Main Activity
                    // position != RecyclerView.NO_POSITION - We check because we have to be sure that doesn't click on item in invalid position
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(notes.get(position));
                    }// In this way we can pass note on position which we clicked
                }
            });
        }
    }

    // Whenever we click on item in our note adapter, we can call this listener. We will implement this method in main activity
    public interface OnItemClickListener {
        void onItemClick(Note note);
    }

    // We can later use this listener variable to call onItemClick method and in this way forward note object to different implements this
    // OnItemListener interface
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

}
