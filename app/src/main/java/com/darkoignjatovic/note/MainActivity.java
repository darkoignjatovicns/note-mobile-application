package com.darkoignjatovic.note;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.darkoignjatovic.note.activity.AddEditNoteActivity;
import com.darkoignjatovic.note.activity.ImagePickerActivity;
import com.darkoignjatovic.note.adapter.NoteAdapter;
import com.darkoignjatovic.note.entity.Note;
import com.darkoignjatovic.note.utils.NoteUtils;
import com.darkoignjatovic.note.viewModel.NoteViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // It's important that these two values are different
    public static final int ADD_NOTE_REQUEST = 1;
    public static final int EDIT_NOTE_REQUEST = 2;

    private NoteViewModel noteViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton buttonAddNote = findViewById(R.id.button_add_note);
        buttonAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we click button we have to pass main Activity and class which we want to open( AddEditNoteActivity.class)
                // So when we click on button open AddEditNoteActivity
                Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                startActivityForResult(intent,ADD_NOTE_REQUEST); // We will later get intent back - WE WILL GET RESULT BACK
                // request code(second parameter) -  We could call startActivityForResult for different activities. We will check result with that request code later
            }
        });


        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this)); // Every RecyclerView have layout manager
        recyclerView.setHasFixedSize(true); // You should set true if the recyclerView size want change

        final NoteAdapter adapter = new NoteAdapter();
        recyclerView.setAdapter(adapter);

        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        /**
         * observe() - life data method and we have to pass two arguments.
         * First is lifecycle owner (activity or fragment).
         * Second is instance of object which we observe
         */
        noteViewModel.getAllNotes().observe(this, new Observer<List<Note>>() {
            // This method will be triggerd every time the data in our life databa object was changed
            @Override
            public void onChanged(List<Note> notes) {
                // update RecyclerView
                // This method should be triggered as soon as we start observing
                // Every time onChange will be triggered in the time when the data in the corresponding note table changes
                // Our adapter should be updated with new list of notes
                adapter.setNotes(notes);
            }
        });

        // This is a class that will make our recyclerView swipable, and we have to pass callback
        // In SimpleCallback we have to pass values. This values is defined directions we want support
        // So we want support left and right directions. We put 0 for drag and drop because we don't want support that functionality
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            // onMove - drag and drop functionality
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            // For swiping
            // This is the viewHolder we swiped of(first parameter)
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                noteViewModel.delete(adapter.getNoteAt(viewHolder.getAdapterPosition()));
                // After delete we don't have to update anything because that will do automatically
                Toast.makeText(MainActivity.this, "Note deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView); // we passed Recycler View object because otherwise this way wouldn't work


        // UPDATE Note
        // We will pass anonymous class which implements OnItemClickListener interface and we will define onItemClick method.
        // So we will set in adapter this interface.
        adapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {
            // note parameter - this is the note we clicked on our recycler view
            @Override
            public void onItemClick(Note note) {
                // Second parameter is the class we want to open
                Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                // We need id when we update note
                intent.putExtra(AddEditNoteActivity.EXTRA_ID, note.getId());
                intent.putExtra(AddEditNoteActivity.EXTRA_TITLE, note.getTitle());
                intent.putExtra(AddEditNoteActivity.EXTRA_DESCRIPTION, note.getDescription());
                intent.putExtra(AddEditNoteActivity.EXTRA_IMAGE_PATH, note.getImagePath());
                intent.putExtra(AddEditNoteActivity.EXTRA_IMAGE_NAME, note.getImageName());
                intent.putExtra(AddEditNoteActivity.EXTRA_LATITUDE, note.getLatitude());
                intent.putExtra(AddEditNoteActivity.EXTRA_LONGITUDE, note.getLongitude());
                startActivityForResult(intent, EDIT_NOTE_REQUEST);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_NOTE_REQUEST && resultCode == RESULT_OK) {
            String title = data.getStringExtra(AddEditNoteActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEditNoteActivity.EXTRA_DESCRIPTION);
            double latitude = data.getDoubleExtra(AddEditNoteActivity.EXTRA_LATITUDE, 0.0);
            double longitude = data.getDoubleExtra(AddEditNoteActivity.EXTRA_LONGITUDE, 0.0);
            String address = data.getStringExtra(AddEditNoteActivity.EXTRA_ADDRESS);
            String imagePath = data.getStringExtra(AddEditNoteActivity.EXTRA_IMAGE_PATH);
            String imageName = data.getStringExtra(AddEditNoteActivity.EXTRA_IMAGE_NAME);

            Note note = new Note(title, description);
            note.setLatitude(latitude);
            note.setLongitude(longitude);
            note.setAddress(address);
            note.setImagePath(imagePath);
            note.setImageName(imageName);
            note.setCreated(NoteUtils.currentDateString());
            noteViewModel.insert(note);

            Toast.makeText(this, "ImagePath:" + imagePath + "\nImageName: " + imageName , Toast.LENGTH_SHORT).show();
          //  Toast.makeText(this, "Note saved", Toast.LENGTH_SHORT).show();
        } else if (requestCode == EDIT_NOTE_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AddEditNoteActivity.EXTRA_ID, -1);

            // This means something went wrong
            // It shouldn't happen in our case but we need take care about it
            if (id == -1) {
                Toast.makeText(this, "Note can't be updated", Toast.LENGTH_SHORT).show();
                return; // Leaves method
            }


            String title = data.getStringExtra(AddEditNoteActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEditNoteActivity.EXTRA_DESCRIPTION);
            double latitude = data.getDoubleExtra(AddEditNoteActivity.EXTRA_LATITUDE, 0.0);
            double longitude = data.getDoubleExtra(AddEditNoteActivity.EXTRA_LONGITUDE, 0.0);
            String address = data.getStringExtra(AddEditNoteActivity.EXTRA_ADDRESS);
            String imagePath = data.getStringExtra(AddEditNoteActivity.EXTRA_IMAGE_PATH);
            String imageName = data.getStringExtra(AddEditNoteActivity.EXTRA_IMAGE_NAME);

            Note note = new Note(title, description);
            note.setCreated(NoteUtils.currentDateString());
            note.setId(id); // Important: If we forget this line the update operation won't be happened because room can't be identified entry
            note.setImagePath(imagePath);
            note.setImageName(imageName);
            note.setAddress(address);
            note.setLatitude(latitude);
            note.setLongitude(longitude);
            noteViewModel.update(note);

            Toast.makeText(this, "Note updated", Toast.LENGTH_SHORT).show();
        } else {
            // This case will be when we click X (cancel)
            Toast.makeText(this, "Note not saved", Toast.LENGTH_SHORT).show();
        }
    }

    // Custom menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true; // true - that will be showed, false - that won't be showed
    }

    // When we select one of menu items on menu in this activity
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Check which item was selected
        switch(item.getItemId()) {
            case R.id.delete_all_notes:
                noteViewModel.deleteAllNotes();
                Toast.makeText(this, "All notes was deleted", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.choose_image:
                Intent intent = new Intent(MainActivity.this, ImagePickerActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
