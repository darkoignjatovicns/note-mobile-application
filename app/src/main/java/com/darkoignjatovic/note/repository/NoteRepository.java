package com.darkoignjatovic.note.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.darkoignjatovic.note.dao.NoteDao;
import com.darkoignjatovic.note.database.NoteDatabase;
import com.darkoignjatovic.note.entity.Note;

import java.util.List;

public class NoteRepository {

    private NoteDao noteDao;
    private LiveData<List<Note>> allNote;

    // We use that context to create our database instance
    public NoteRepository(Application application) {
        NoteDatabase database = NoteDatabase.getInstance(application);
        noteDao = database.noteDao();
        allNote = noteDao.getAllNotes();
    }

    /**
     * For our database operations we have to execute call on the background ourselves
     * because Room doesn't allowed database operation on the main thread
     * if you try to do this our app will be crushed
     */


    // These methods are the API that repository exposes to the outside

    public void insert(Note note) {
        new InsertNoteAsyncTask(noteDao).execute(note);
    }

    public void update(Note note) {
        new UpdateNoteAsyncTask(noteDao).execute(note);
    }

    public void delete(Note note) {
        new DeleteNoteAsyncTask(noteDao).execute(note);
    }

    public void deleteAllNotes() {
        new DeleteAllNotesAsyncTask(noteDao).execute();
    }

    // Room will be automatically executed database operations set returns "LiveData" on the background thread, so we don't
    // have to take care of this
    public LiveData<List<Note>> getAllNotes() {
        return allNote;
    }


    // INSERT NOTE
    private static class InsertNoteAsyncTask extends AsyncTask <Note, Void ,Void > {

        // We need noteDao to make database operations
        private NoteDao noteDao;

        // Since the class is static we can't access noteDao in this repository directly
        // So we have to passed in our constructor
        private InsertNoteAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        // Mandatory method, we have to implement. Other methods are optional
        @Override
        protected Void doInBackground(Note... notes) { // This parameter is boxes
            // Because we don't get passed notes, we pass only one note
            noteDao.insert(notes[0]);
            return null;
        }
    }

    // UPDATE NOTE
    private static class UpdateNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao noteDao;

        private UpdateNoteAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.update(notes[0]);
            return null;
        }
    }

    //DELETE NOTE
    private static class DeleteNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao noteDao;

        private DeleteNoteAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }


        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.delete(notes[0]);
            return null;
        }
    }

    //DELETE ALL NOTES
    private  static class DeleteAllNotesAsyncTask extends AsyncTask<Void, Void, Void> {

        private NoteDao noteDao;

        private DeleteAllNotesAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.deleteAllNotes();
            return null;
        }
    }










}
