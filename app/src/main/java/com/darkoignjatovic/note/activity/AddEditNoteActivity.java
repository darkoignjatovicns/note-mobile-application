package com.darkoignjatovic.note.activity;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.darkoignjatovic.note.BuildConfig;
import com.darkoignjatovic.note.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;

public class AddEditNoteActivity extends AppCompatActivity {

    // The best practice for intent extra key is using the package name to keep it unique
    public static final String EXTRA_ID =
            "com.darkoignjatovic.note.EXTRA_ID";
    public static final String EXTRA_TITLE =
            "com.darkoignjatovic.note.EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION =
            "com.darkoignjatovic.note.EXTRA_DESCRIPTION";
    public static final String EXTRA_LATITUDE=
            "com.darkoignjatovic.note.EXTRA_LATITUDE";
    public static final String EXTRA_LONGITUDE =
            "com.darkoignjatovic.note.EXTRA_LONGITUDE";
    public static final String EXTRA_ADDRESS =
            "com.darkoignjatovic.note.EXTRA_ADDRESS";
    public static final String EXTRA_IMAGE_PATH =
            "com.darkoignjatovic.note.IMAGE_PATH";
    public static final String EXTRA_IMAGE_NAME =
            "com.darkoignjatovic.note.IMAGE_NAME";


    // check variables
    int updateCheck;
    int newPhotoCheck;

    private EditText editTextTitle;
    private EditText editTextDescription;
    private String mAddressLabel;
    private TextView mAddressText;
    private Button mMapButton;
    private FloatingActionButton takePhotoButton;
    private ImageView imageView;
   // private Button noteLocationButton;

    private double latitude;
    private double longitude;
    private String address;
    List<Address> addresses;
    Bitmap imageBitmap;
    private String imageName;
    private String imagePath;

    FileOutputStream outputStream;

    private static final String TAG = AddEditNoteActivity.class.getSimpleName();

    private static final int REQUEST_IMAGE_CAPTURE = 101;

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * Provides the entry point to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;

    Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);


        updateCheck = 0;
        newPhotoCheck = 0;

        editTextTitle = findViewById(R.id.edit_text_title);
        editTextDescription = findViewById(R.id.edit_text_description);

        //Camera
        imageView = findViewById(R.id.imageView);
        takePhotoButton = findViewById(R.id.button_take_photo);
        takePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent galleryIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(galleryIntent,REQUEST_IMAGE_CAPTURE);

            }
        });



        // Location
        mAddressLabel = "Address";
        mAddressText = (TextView) findViewById((R.id.address_text));
        mMapButton = (Button) findViewById(R.id.mapButton);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        geocoder = new Geocoder(this, Locale.getDefault());


        // End Location

        // To order to get X in the top left corner
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);


        // This is how we prepare our AddEditNoteActivity when we open it
        Intent intent= getIntent();
        // This part only will be triggered if intent contains id
        if(intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit Note");
           // noteLocationButton = (Button) findViewById(R.id.noteLocationButton);
            // We also want to  fill edit text fields with value
            editTextTitle.setText(intent.getStringExtra(EXTRA_TITLE));
            editTextDescription.setText(intent.getStringExtra(EXTRA_DESCRIPTION));
            imagePath = intent.getStringExtra(EXTRA_IMAGE_PATH);
            imageName =  intent.getStringExtra(EXTRA_IMAGE_NAME);
            loadImageFromStorage(imagePath);
            updateCheck = 1;
           /* final double noteLatitude = intent.getDoubleExtra(EXTRA_LATITUDE, 0);
            final double noteLongitude = intent.getDoubleExtra(EXTRA_LONGITUDE, 0);
            noteLocationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent data = new Intent(AddEditNoteActivity.this, NoteLocation.class);
                    data.putExtra(EXTRA_LATITUDE, noteLatitude);
                    data.putExtra(EXTRA_LONGITUDE, noteLongitude);
                    startActivity(data);

                }
            });*/

        } else {
            setTitle("Add Note"); // This text will be added to the action bar

        }

    }

    @Override
    public void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            requestPermissions();
        } else {
             getLastLocation();
            // Link UI elements to actions in code
            mMapButton.setOnClickListener(new View.OnClickListener() {

                // Called when user clicks the Show Map button
                public void onClick(View v) {
                    try {

                        // Process text for network transmission
                        address = address.replace(' ', '+');

                        // Create Intent object for starting Google Maps application
                        Intent geoIntent = new Intent(
                                android.content.Intent.ACTION_VIEW, Uri
                                .parse("geo:0,0?q=" + address));

                        // Use the Intent to start Google Maps application using Activity.startActivity()
                        startActivity(geoIntent);

                    } catch (Exception e) {
                        // Log any error messages to LogCat using Log.e()
                        Log.e(TAG, e.toString());
                    }
                }
            });
        }
    }

    private void saveNote() {
        String title = editTextTitle.getText().toString();
        String description = editTextDescription.getText().toString();
      //  getLastLocation();

        // So title and description can't be empty, if these fields are empty we're not allowed to pass
        if(title.trim().isEmpty() || description.trim().isEmpty()) {
            Toast.makeText(this , "Please insert a title and description", Toast.LENGTH_SHORT).show();
            return;
        }

        // Now we accept input fields and save our note in our database and close activity
        // We send data when we start the activity
        // We need Intent to send data back to the activity that started this one
        Intent data = new Intent();
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_DESCRIPTION, description);
        data.putExtra(EXTRA_LATITUDE, latitude);
        data.putExtra(EXTRA_LONGITUDE, longitude);
        data.putExtra(EXTRA_ADDRESS,address);
        System.out.println("New Photo Check: " + newPhotoCheck);
        data.putExtra(EXTRA_IMAGE_PATH, newPhotoCheck > 0 ? saveToInternalStorage(imageBitmap) : imagePath);
        data.putExtra(EXTRA_IMAGE_NAME, imageName);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if(id != -1) {
            // Only in this the case we want to put id in intent
            data.putExtra(EXTRA_ID, id);
        }

        //String absolutePath = saveToInternalStorage(imageBitmap);

        // Then we call setResult(), that is indicated if the input successful or not
        // We passed and data intent
        // We can read these two values on mainActivity and this is the way how we check is everything correctly
        setResult(RESULT_OK, data);
      //  savePhoto();

        finish(); // Then we call finish to close activity
    }

    // Custom menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // This simple set system to use our note_menu as the menu of the activity
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note_menu, menu);
        // We want to display menu
        return true;
    }


    // When we click on menu icon
    /**
     * @param item  - It's menu item that was clicked
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // We get which one was clicked
        switch (item.getItemId()) {
            case R.id.save_note:
                saveNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // CAMERA
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
            newPhotoCheck = 1;
        }
    }


    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
       // File directory = cw.getDir(Environment.getExternalStorageState()+"/NotePhotos/", Context.MODE_PRIVATE);
        // Create imageDir
        imageName = System.currentTimeMillis()+".jpg";
        File mypath=new File(directory,imageName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    private void loadImageFromStorage(String path)
    {

        try {
            File f=new File(path, imageName);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
           // ImageView img=(ImageView)findViewById(R.id.imageView);
            imageView.setImageBitmap(b);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }


    // LOCATION
    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     * <p>
     * Note: this method should be called after location permission has been granted.
     */
    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                        @Override
                        public void onComplete(@NonNull Task<Location> task) {
                            if (task.isSuccessful() && task.getResult() != null) {
                                mLastLocation = task.getResult();

                                latitude = mLastLocation.getLatitude();
                                longitude = mLastLocation.getLongitude();

                                try {
                                    // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                    addresses = geocoder.getFromLocation(latitude, longitude, 1);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                address = addresses.get(0).getAddressLine(0);
                                mAddressText.setText(String.format(Locale.ENGLISH, "%s: %s",
                                        mAddressLabel,
                                        address));

                            } else {
                                Log.w(TAG, "getLastLocation:exception", task.getException());
                                showSnackbar(getString(R.string.no_location_detected));
                            }
                        }
                    });
    }

    /**
     * Shows a {@link Snackbar} using {@code text}.
     *
     * @param text The Snackbar text.
     */
    private void showSnackbar(final String text) {
        View container = findViewById(R.id.add_edit_activity_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(AddEditNoteActivity.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                         BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

}
