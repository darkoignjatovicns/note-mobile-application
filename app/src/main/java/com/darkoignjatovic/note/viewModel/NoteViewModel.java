package com.darkoignjatovic.note.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.darkoignjatovic.note.entity.Note;
import com.darkoignjatovic.note.repository.NoteRepository;

import java.util.List;

// AndroidViewModel is subclass of ViewModel
public class NoteViewModel extends AndroidViewModel {

    private NoteRepository repository;
    // Cache in main activity
    private LiveData<List<Note>> allNotes; // This will be initialized when we initialize NoteViewModel

    // The difference between AndroidViewModel is that we pass application context in constructor
    public NoteViewModel(@NonNull Application application) {
        super(application);
        repository = new NoteRepository(application);
        allNotes = repository.getAllNotes();
    }

    public void insert(Note note) {
        repository.insert(note);
    }

    public void update(Note note) {
        repository.update(note);
    }

    public void delete(Note note) {
        repository.delete(note);
    }

    public void deleteAllNotes() {
        repository.deleteAllNotes();
    }

    public LiveData<List<Note>> getAllNotes() {
        return repository.getAllNotes();
    }


}
